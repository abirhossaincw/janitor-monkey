from function_janitor_monkey import handler


class TestApp:
    def test_get_suggested_tags(self, lambdaa_function_name):

        # Arrange
        expected_tags = {"environment": "production"}

        # Act
        actual_tags = handler.get_suggested_tags(lambdaa_function_name)

        # Assert
        assert actual_tags == expected_tags
