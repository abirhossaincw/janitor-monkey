## Deploy DEV Infrastructure using Terragrunt

**1. Build deployment package**

In the container, run the following. A .zip deployment package will be created in /function_janitor_monkey

```bash
scripts/build.sh
```

**2. Deploy using Terragrunt**

In the container, run the following

```bash
1. cd terraform/live/dev
2. terragrunt run-all apply
```

**4. Delete deployed resources**

```bash
1. terragrunt destroy-all
```

---

### All `make` commands

| Command | Action |
| ------- | ------ |
| `lint` | Run the linter to verify code style. |
| `format` | Auto-format the code using `black`. |
| `test` | Run the test suite. |
| `test-watch` | Run the test suite and watch for changes. |
| `test-coverage` | Run the test suite and generate coverage reports. |
| `apidocs` | Generate python docs. |
