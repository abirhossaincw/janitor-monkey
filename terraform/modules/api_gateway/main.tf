# Resources

resource "aws_api_gateway_rest_api" "api" {
  name = "${var.app_name}-gateway"
  body = var.body
}

resource "aws_api_gateway_deployment" "api" {
  rest_api_id = aws_api_gateway_rest_api.api.id

  triggers = {
    # trigger deployment on every openapi.yaml change
    openapi_checksum = md5(var.body)
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lambda_permission" "lambda" {
  function_name = var.function_name
  action        = "lambda:InvokeFunction"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.api.execution_arn}/*"
}

resource "aws_api_gateway_stage" "api" {
  stage_name = var.environment
  deployment_id = aws_api_gateway_deployment.api.id
  rest_api_id = aws_api_gateway_rest_api.api.id
}


