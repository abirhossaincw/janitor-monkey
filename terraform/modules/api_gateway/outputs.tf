output "aws_api_gateway_execution_arn" {
  value       = aws_api_gateway_rest_api.api.execution_arn
  description = "The execution ARN part to be used in lambda_permission's source_arn when allowing API Gateway to invoke a Lambda function"
}

output "aws_api_gateway_invoke_url" {
  value       = aws_api_gateway_deployment.api.invoke_url
  description = "The URL to invoke the API pointing to the stage"
}