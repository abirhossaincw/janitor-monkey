# aws_api_gateway_rest_api
variable "app_name" {
  description = "The app name"
  type        = string
}

variable "body" {
  description = "An OpenAPI specification that defines the REST API"
  type        = string
}

# aws_api_gateway_deployment
variable "environment" {
  description = "The environment"
  type        = string
}

# aws_lambda_permission
variable "function_name" {
  description = "Lambda function name"
  type        = string
}

variable "xray_tracing_enabled" {
  description = "Whether active tracing with X-ray is enabled. Defaults to false"
  type        = string
  default     = false
}