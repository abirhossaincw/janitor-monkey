variable "account_bootstrap_tf_statefile_bucket" {
  type = string
}

variable "aws_account_name" {
  type = string
}

variable "aws_region" {
  type = string
}