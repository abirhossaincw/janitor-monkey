data "terraform_remote_state" "account_bootstrap" {
    backend = "s3"
    config = {
        bucket  = var.account_bootstrap_tf_statefile_bucket
        key     = var.aws_account_name
        region  = var.aws_region
    }
}