# Create an IAM role
resource "aws_iam_role" "role" {
  name                = var.name
  assume_role_policy  = data.aws_iam_policy_document.role.json
}

# Specify which services can assume this role
data "aws_iam_policy_document" "role" {

  statement {
    principals {
      type        = "Service"
      identifiers = var.assume_role_services
    }

    actions = ["sts:AssumeRole"]
  }
}

# Loop over each policy arn and attach it to the role
resource "aws_iam_role_policy_attachment" "this" {
  for_each    = toset(var.policy_arns)
  role        = aws_iam_role.role.name
  policy_arn  = each.value
}