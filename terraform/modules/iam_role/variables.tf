# aws_iam_role
variable "name" {
  type = string
}

# aws_iam_policy_document
variable "assume_role_services" {
  type = list(string)
}

# A list of policy arns to attach to this role
variable "policy_arns" {
  type = list(string)
  default = []
}