output "aws_iam_role_name" {
  value       = aws_iam_role.role.name
  description = "The name of the role created"
}

output "aws_iam_role_arn" {
  value       = aws_iam_role.role.arn
  description = "The arn of the role created"
}