variable "name" {
  description = "The name of the policy"
  type        = string
}

variable "description" {
  description = "Description of the IAM policy"
  type        = string
  default     = ""
}

variable "actions" {
  description = "A list of actions that this statement either allows or denies"
  type        = list(string)
}

variable "resources" {
  description = "A list of resource ARNs that this statement applies to"
  type        = list(string)
}

variable "effect" {
  description = "Either 'Allow' or 'Deny', to specify whether this statement allows or denies the given actions"
  type        = string
  default     = "Allow"
}

variable "role_name" {
  description = "The role name to attach the policy to"
  type        = string
}