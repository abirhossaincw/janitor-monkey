data "aws_iam_policy_document" "this" {
  statement {
    actions = var.actions
    resources = var.resources
    effect = var.effect
  }
}