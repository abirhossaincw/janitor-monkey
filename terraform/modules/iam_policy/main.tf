# Create a policy - Cloudwatch Log write access
resource "aws_iam_policy" "this" {
  name = var.name
  description = var.description

  policy = data.aws_iam_policy_document.this.json
}

resource "aws_iam_role_policy_attachment" "this" {
  role       = var.role_name
  policy_arn = aws_iam_policy.this.arn
}