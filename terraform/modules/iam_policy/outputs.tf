output "aws_iam_policy_name" {
  value       = aws_iam_policy.this.name
  description = "The name of the policy created"
}

output "aws_iam_policy_arn" {
  value       = aws_iam_policy.this.arn
  description = "The arn of the policy created"
}