# aws_lambda
variable "function_name" {
  type  = string
}

variable "runtime" {
  type  = string
}

variable "handler" {
  type  = string
}

variable "memory_size" {
  type  = number
}

variable "timeout" {
  type  = number
}

variable "source_code_hash" {
  type  = string
}

variable "filename" {
  type  = string
}

variable "role" {
  type  = string
}

variable "environment_variables" {
  type  = map(string)
  default = {}
}

variable "mode" {
  description = "Can be either PassThrough or Active"
  type        = string
  default     = "PassThrough"
}