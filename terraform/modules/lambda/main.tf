# Resources
resource "aws_lambda_function" "lambda" {
  function_name     = var.function_name
  runtime           = var.runtime
  handler           = var.handler
  memory_size       = var.memory_size
  timeout           = var.timeout
  source_code_hash  = var.source_code_hash
  filename          = var.filename
  role              = var.role

  environment {
    variables = var.environment_variables
  }

  tracing_config {
    mode = var.mode
  }
}

resource "aws_cloudwatch_log_group" "lambda" {
  name = "/aws/${var.function_name}/lambda"
  retention_in_days = 7
}