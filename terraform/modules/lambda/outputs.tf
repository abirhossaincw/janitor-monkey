output "aws_lambda_function_name" {
  value       = aws_lambda_function.lambda.function_name
  description = "The lambda function name"
}

output "aws_lambda_invoke_arn" {
  value       = aws_lambda_function.lambda.invoke_arn
  description = "The ARN to be used for invoking Lambda Function from API Gateway"
}

output "aws_lambda_cloudwatch_log_group_arn" {
  value       = aws_cloudwatch_log_group.lambda.arn
  description = "The ARN to Lambda CloudWatch log group"
}