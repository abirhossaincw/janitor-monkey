locals {
  aws_region = "ap-southeast-2"
  app_name  = get_env("BITBUCKET_REPO_SLUG", "janitor-monkey")
  build_artifact_file_path = "${get_env("BITBUCKET_CLONE_DIR", "")}/function_janitor_monkey/deployment-package.zip"
}