locals {
  # Automatically load environment-level variables
  common_vars       = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  environment_vars  = read_terragrunt_config(find_in_parent_folders("environment.hcl"))

  # Extract out common variables for reuse
  app_name     = local.common_vars.locals.app_name

  # Extract out environment variables for reuse
  environment  = local.environment_vars.locals.environment
}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../modules/api_gateway"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

dependency "lambda" {
  config_path = "../lambda"

  mock_outputs = {
    aws_lambda_function_name = "dummy-aws-lambda-function-name"
    aws_lambda_invoke_arn    = "dummy-aws-lambda-invoke-arn"
  }
}

inputs = {
  # aws_api_gateway_rest_api
  app_name = local.app_name
  body = templatefile("../../../../docs/openapi.yaml", {
    janitor_monkey_function_invoke_arn = dependency.lambda.outputs.aws_lambda_invoke_arn
  })

  # aws_api_gateway_deployment
  environment = local.environment

  # aws_lambda_permission
  function_name = dependency.lambda.outputs.aws_lambda_function_name

  xray_tracing_enabled = true
}