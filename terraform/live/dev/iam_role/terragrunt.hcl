locals {
  # Automatically load environment-level variables
  common_vars       = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  environment_vars  = read_terragrunt_config(find_in_parent_folders("environment.hcl"))

  # Extract out common variables for reuse
  aws_region  = local.common_vars.locals.aws_region
  app_name    = local.common_vars.locals.app_name

  # Extract out environment variables for reuse
  aws_account_name  = local.environment_vars.locals.aws_account_name
  aws_account_id    = local.environment_vars.locals.aws_account_id
  environment       = local.environment_vars.locals.environment
}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../modules/iam_role"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

inputs = {
  # aws_iam_role
  name = "${local.app_name}-lambda"

  assume_role_services = ["lambda.amazonaws.com"]

  policy_arns = [
    "arn:aws:iam::aws:policy/AdministratorAccess"
  ]
}