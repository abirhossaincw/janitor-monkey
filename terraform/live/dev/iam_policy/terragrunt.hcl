locals {
  # Automatically load environment-level variables
  common_vars       = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  environment_vars  = read_terragrunt_config(find_in_parent_folders("environment.hcl"))

  # Extract out common variables for reuse
  app_name  = local.common_vars.locals.app_name

  # Extract out environment variables for reuse
  aws_account_id  = local.environment_vars.locals.aws_account_id
  environment     = local.environment_vars.locals.environment
}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../modules/iam_policy"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

dependency "lambda" {
  config_path = "../lambda"

  mock_outputs = {
    aws_lambda_cloudwatch_log_group_arn = "dummy-aws-lambda-cloudwatch-log-group-arn"
  }
}

dependency "api_gateway" {
  config_path = "../api_gateway"
}

dependency "iam_role" {
  config_path = "../iam_role"

  mock_outputs = {
    aws_iam_role_name = "dummy-aws-iam-role-name"
  }
}

inputs = {
  # aws_iam_policy
  name = "${local.app_name}-cloudwatch-logs-write-only-access"
  description = "CloudWatch logs write access policy for janitor-monkey specific log groups"

  actions = [
    "logs:PutLogEvents"
  ]

  resources = [
    dependency.lambda.outputs.aws_lambda_cloudwatch_log_group_arn
  ]

  role_name = dependency.iam_role.outputs.aws_iam_role_name
}