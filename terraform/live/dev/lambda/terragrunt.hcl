locals {
  # Automatically load environment-level variables
  common_vars      = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl"))

  # Extract out common variables for reuse
  function_name             = "${local.common_vars.locals.app_name}-${local.environment_vars.locals.environment}-updated"
  build_artifact_file_path  = local.common_vars.locals.build_artifact_file_path
}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../modules/lambda"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

dependency "iam_role" {
  config_path = "../iam_role"

  mock_outputs = {
    aws_iam_role_arn = "dummy-aws-iam-role-arn"
  }
}

inputs = {
  # aws_lambda
  function_name         = local.function_name
  runtime               = "python3.7"
  handler               = "handler.lambda_handler"
  memory_size           = 128
  timeout               = 60
  source_code_hash      = "${filebase64sha256(local.build_artifact_file_path)}"
  filename              = "${local.build_artifact_file_path}"
  role                  = dependency.iam_role.outputs.aws_iam_role_arn
  mode                  = "Active"

  environment_variables = {
      TEST_ENV_VAR = "dummy-var"
    }

}