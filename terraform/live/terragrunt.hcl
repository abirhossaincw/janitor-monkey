# ---------------------------------------------------------------------------------------------------------------------
# TERRAGRUNT CONFIGURATION
# Terragrunt is a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules,
# remote state, and locking: https://github.com/gruntwork-io/terragrunt
# ---------------------------------------------------------------------------------------------------------------------

locals {
  # Automatically load environment variables
  common_vars       = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  environment_vars  = read_terragrunt_config(find_in_parent_folders("environment.hcl"))

  # Extract the variables we need for easy access
  aws_region        = local.common_vars.locals.aws_region
  app_name          = local.common_vars.locals.app_name
  environment       = local.environment_vars.locals.environment
  aws_account_name  = local.environment_vars.locals.aws_account_name
  aws_account_id    = local.environment_vars.locals.aws_account_id

  assume_role_arn = "arn:aws:iam::${local.aws_account_id}:role/terraform"
}

# Generate an AWS provider block
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region = "${local.aws_region}"

  assume_role {
    role_arn = "${local.assume_role_arn}"
  }

  # Only these AWS Account IDs may be operated on by this template
  allowed_account_ids = ["${local.aws_account_id}"]
}
EOF
}

# Configure Terragrunt to automatically store tfstate files in an S3 bucket
remote_state {
  backend = "s3"
  config = {
    encrypt        = true
    bucket         = "terraform-${local.aws_account_name}"
    key            = "${local.app_name}/${path_relative_to_include()}/terraform.tfstate"
    region         = local.aws_region
    dynamodb_table = "terraform-lock-fda"
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}