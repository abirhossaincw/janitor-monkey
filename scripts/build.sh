#!/bin/bash
set -euo pipefail

# Create a lambda deployment package zip named 'deployment.zip' in janitor-monkey
JANITOR_MONKEY_FUNCTION_FOLDER=function_janitor_monkey

echo
echo "Serverless: Build janitor-monkey artifacts to '.serverless/' directory"

pip install -q -r $JANITOR_MONKEY_FUNCTION_FOLDER/requirements.txt -t $JANITOR_MONKEY_FUNCTION_FOLDER/.serverless/tmp --no-deps
cp $JANITOR_MONKEY_FUNCTION_FOLDER/handler.py $JANITOR_MONKEY_FUNCTION_FOLDER/.serverless/tmp
(cd $JANITOR_MONKEY_FUNCTION_FOLDER/.serverless/tmp; zip -9 -q -r ../../deployment-package.zip * -x \*pycache* \*.dist-info*)

rm -rf $JANITOR_MONKEY_FUNCTION_FOLDER/.serverless/tmp

# Echo the zip files
echo
echo "Completed creating deployment package."
echo
ls -lah $JANITOR_MONKEY_FUNCTION_FOLDER/*.zip