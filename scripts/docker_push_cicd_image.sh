#!/bin/sh
# Use this script to build and push the docker image to Dockerhub. Be mindful that this image is used by
# bitbucket pipelines and any changes might break the deploy process.

set -ex

GIT_BUILD_HASH=$(git rev-parse --short HEAD)

# Build docker image with multiple tags
docker build --no-cache --file ./docker/Dockerfile.cicd --tag abirhossaincw/janitor-monkey:cicd-latest --tag abirhossaincw/janitor-monkey:cicd-${GIT_BUILD_HASH} .

# Push docker image to Dockerhub
docker push abirhossaincw/janitor-monkey:cicd-latest
docker push abirhossaincw/janitor-monkey:cicd-${GIT_BUILD_HASH}