#!/bin/bash
set -euo pipefail

JANITOR_MONKEY_FUNCTION_FOLDER=function_janitor_monkey

build_artifact_base_uri="s3://${BUILD_ARTIFACTS_BUCKET}/${BITBUCKET_REPO_SLUG}/build-${BITBUCKET_BUILD_NUMBER}"

echo
echo "AWS CLI: Upload lambda code zip files from '.serverless/' directory to '${build_artifact_base_uri}'"
aws s3 cp ${JANITOR_MONKEY_FUNCTION_FOLDER}/ "${build_artifact_base_uri}" --recursive --exclude "*" --include '*.zip' --exclude ".chalice*" --no-progress
