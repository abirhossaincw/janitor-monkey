#!/bin/bash
set -euo pipefail

if [[ $# -eq 0 ]]; then
  echo "Error: Please provide an environment argument to deploy to: 'dev', 'stg' or 'prod'"
  exit 0
fi

echo
echo "Move into /terraform/live/$1"
cd terraform/live/$1
echo
echo "Terragrunt apply-all"
terragrunt apply-all --terragrunt-non-interactive

echo '   _____                             _ '
echo '  / ____|                           | |'
echo ' | (___  _   _  ___ ___ ___  ___ ___| |'
echo '  \___ \| | | |/ __/ __/ _ \/ __/ __| |'
echo '  ____) | |_| | (_| (_|  __/\__ \__ \_|'
echo ' |_____/ \__,_|\___\___\___||___/___(_)'
echo