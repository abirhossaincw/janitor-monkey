import json
import boto3
from tabulate import tabulate

lambda_client = boto3.client("lambda", region_name="ap-southeast-2")


def lambda_handler(event, context):

    #  Get Lambda functions
    lambda_functions = get_lambda_functions(50)

    # return {
    #     'statusCode': 200,
    #     'body': json.dumps(lambda_functions)
    # }

    #  Prepare dataset
    lambda_functions_dataset = get_lambda_functions_dataset(lambda_functions)

    # Prepare result rows and headers
    rows = [
        lambda_function_record.values()
        for lambda_function_record in lambda_functions_dataset
    ]
    headers = lambda_functions_dataset[0].keys()

    # Return response in table format
    return {"statusCode": 200, "body": tabulate(rows, headers, tablefmt="fancy_grid")}


def get_lambda_functions(limit):

    response = lambda_client.list_functions(MaxItems=limit)

    return response["Functions"]


def get_lambda_functions_dataset(lambda_functions):

    lambda_functions_dataset = []

    for lambda_function in lambda_functions:
        function_name = lambda_function["FunctionName"]
        function_runtime = lambda_function["Runtime"]
        function_role = lambda_function["Role"]

        suggested_tags = get_suggested_tags(function_name)

        lambda_functions_dataset.append(
            {
                "Lambda Function Name": function_name,
                "Function Runtime": function_runtime,
                "Suggested Tags": suggested_tags,
            }
        )

    return lambda_functions_dataset


def get_suggested_tags(function_name):
    suggested_tags = {}

    environments = {
        "prod": "production",
        "uat": "uat",
        "test": "test",
        "demo": "demo",
        "dev": "development",
    }

    for key in environments:
        if key in function_name.lower():
            suggested_tags["environment"] = environments[key]

    if len(suggested_tags) == 0:
        return "no tags found"

    return suggested_tags
