lint:
	black --check /app

format:
	black /app

test:
	pytest

test-watch:
	pytest-watch

test-coverage:
	pytest --cov=function_janitor_monkey